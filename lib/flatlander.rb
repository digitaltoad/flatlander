$:.unshift(File.join(File.dirname(__FILE__), 'flatlander'))
require 'fixed_width'

module Flatlander
  def self.included(base)
    base.send(:include, FixedWidth)
  end
end
