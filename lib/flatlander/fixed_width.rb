module Flatlander
  module FixedWidth
    def self.included(base)
      base.extend(ClassMethods)
      base.send(:include, InstanceMethods)
    end

    module ClassMethods
      def from_fw(filepath)
        objects = parse_fw_file(filepath)
        return objects if objects.size > 1
        return objects.first
      end

      def from_fw!(filepath)
        objects = parse_fw_file(filepath)
        return save_fw_objects(objects) if objects.is_a?(Array)
        return save_fw_objects([objects])
      end

      private

      def parse_fw_file(filepath)
        file = File.open(filepath)
        objects = []
        file.each do |line|
          next if file.lineno == 1 && @fw_template.skips.include?(:first)
          next if file.eof? && @fw_template.skips.include?(:last)
          next if @fw_template.skips.include?(file.lineno)
          objects << parse_fw_line(line)
        end
        objects
      end

      def parse_fw_line(line)
        attributes = {}
        @fw_template.fields.each do |key, value|
          attributes[key] = line[fw_offset(value)].to_s.strip
        end
        object = new(attributes)
        object
      end

      def save_fw_objects(objects)
        saved_objects = []
        transaction do
          objects.each do |object|
            object.save
            saved_objects << object
          end
        end
        saved_objects
      end

      def fw_template(&block)
        yield @fw_template ||= Template.new
      end

      def fw_offset(value)
        Range.new(value.first - 1, value.last - 1)
      end
    end

    module InstanceMethods
      def to_fw
        line = ''
        @fw_template.fields.each do |key, value|
          line += ljust(send(:"#{key}"), value.count)
        end
      end
    end

    class Template
      def field(field, range)
        @fields = {} unless @fields
        @fields[field.to_sym] = range
      end

      def fields
        @fields
      end

      def skip(*args)
        @skips = [] unless @skips
        @skips << args
        @skips.flatten!
      end

      def skips
        @skips
      end
    end
  end
end
