ActiveRecord::Base.establish_connection :adapter => 'sqlite3', :database => File.join(File.dirname(__FILE__), 'test.db')

class CreateSchema < ActiveRecord::Migration
  def self.up
    create_table :daily_cardholder_balances, :force => true do |t|
      t.string :record_type
      t.string :client_id
      t.string :transaction_date
      t.string :ending_balance
      t.string :member_id
      t.string :purse_type
      t.string :purse_start
      t.string :purse_end
      t.string :purse_status
    end
  end
end

CreateSchema.suppress_messages { CreateSchema.migrate(:up) }

class DailyCardholderBalance < ActiveRecord::Base
  include Flatlander

  fw_template do |f|
    f.skip  :first, :last
    f.field :record_type,      1..2
    f.field :client_id,        3..8
    f.field :transaction_date, 9..16
    f.field :ending_balance,   17..26
    f.field :member_id,        27..56
    f.field :purse_type,       57..59
    f.field :purse_start,      60..67
    f.field :purse_end,        68..75
    f.field :purse_status,     76..85
  end
end
