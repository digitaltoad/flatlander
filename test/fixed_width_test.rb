require 'test_helper'

class FixedWidthTest < Test::Unit::TestCase
  context "A class which has included Flatlander" do
    setup do
      @class = DailyCardholderBalance
    end

    should "be able to parse file with multiple records" do
      assert ! @class.from_fw('test/flat_files/daily_cardholder_balance').empty?
    end

    should "parse multiple records into an array of @class objects" do
      results = @class.from_fw('test/flat_files/daily_cardholder_balance')
      assert results.first.is_a?(DailyCardholderBalance)
    end

    should "parse and save a 10 record file when bang used" do
      @class.from_fw!('test/flat_files/daily_cardholder_balance')
      assert DailyCardholderBalance.count == 10
    end

    should "parse an object into fixed width format" do
      object = @class.first
      assert object.to_fw.is_a?(String)
    end
  end
end
