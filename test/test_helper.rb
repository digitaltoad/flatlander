$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'rubygems'
require 'test/unit'
require 'activerecord'
require 'shoulda'
require 'mocha'
require 'flatlander'
require 'models'
begin require 'redgreen'; rescue LoadError; end

class Test::Unit::TestCase
end
